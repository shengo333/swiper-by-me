const baseUrl = "http://localhost:4000"


export default async function(route, fields = []) {
    return await useFetch(`${baseUrl}${route}`, {pick: [...fields]})
}